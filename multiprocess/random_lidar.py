from time import sleep
from sys import stdout
from random import random

angle = 0
while True:
	angle %= 360
	print("%d %f" % (angle, 100*random()))
	stdout.flush()

	sleep(2*random())
	angle += 1