#!/usr/bin/env python
# Laptop Server

import socket
import matplotlib
import pylab
import sys
import time
import rangeData
from math import cos,sin

HOST = '192.168.7.2'
PORT = 5010
# Connect to host over port
#sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#sock.bind((HOST, PORT))
#sock.listen(1)

rdData = rangeData.rangeData()

def processData(ans, rangs, errrs):
    def stringToList(l):
        """ String split up by ', ' """
        x = l.split(', ')
        x[0] = x[0][1:]
        last_elem = x[len(x)-1]
        x[len(x)-1] = x[len(x)-1][:len(last_elem)-1]
        x = map(lambda t: int(t) if t != '' else 0, x)
        return x

    angles = stringToList(ans)
    ranges = stringToList(rangs)
    errors = stringToList(errrs)

    # filter out those with errors > 8000
    badDataIndices = []
    for e in range(len(errors)):
        if (errors[e] > 8000) or (ranges[e] > 7000): 
            badDataIndices.append(e)

    for i in badDataIndices:
        angles[i] = -1
        ranges[i] = -1
        errors[i] = -1

    angles = filter(lambda t: t != -1, angles)
    ranges = filter(lambda t: t != -1, ranges)
    errors = filter(lambda t: t != -1, errors)

    rdData.insertData(angles, ranges)
    (angles, ranges) = rdData.getValidData()

    angles = map(lambda a: a*3.14159/180., angles)
    xs = map(lambda i: ranges[i]*cos(angles[i]), range(len(angles)))
    ys = map(lambda i: ranges[i]*sin(angles[i]), range(len(angles)))

    matplotlib.pyplot.scatter(xs,ys)
    matplotlib.pyplot.show()

try:
    # Connect to host over port
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect((HOST, PORT))
except socket.error as err:
    print 'Unable to connect to BeagleBone: ' + str(err)

try:
    sock.send('Hello world, from laptop!')
    allDataReceived = [False, False, False]
    try:
        while True:
            data = sock.recv(32)
            if data == 'Starting Scan':
                while True:
                    data = sock.recv(32)
                    if data == 'Sending Scan Data':
                        while True:
                            data = sock.recv(32)
                            print 'data: ' + data
                            if data == 'Sending Angles':
                                angles = sock.recv(2048)
                                print 'angles:'
                                print angles
                                allDataReceived[0] = True
                                sock.send('got angles')
                                continue
                            elif data == 'Sending Ranges':
                                ranges = sock.recv(2048)
                                done = sock.recv(1024)
                                if done == 'Done sending ranges':
                                    pass
                                else:
                                    done = done[:len(done)-len('Done sending ranges')]
                                    ranges += done
                                print 'ranges:'
                                print ranges
                                allDataReceived[1] = True
                                sock.send('got ranges')
                                continue
                            elif data == 'Sending Errors':
                                errors = sock.recv(2048)
                                #done = sock.recv(1024) #stuck here
                                #if done == 'Done sending errors':
                                #    pass
                                #else:
                                #    errors += done
                                print 'errors:'
                                print errors
                                allDataReceived[2] = True
                                sock.send('got errors')
                                break
                        break
            if allDataReceived == [True, True, True]:
                processData(angles, ranges, errors)
                allDataReceived = [False, False, False]
                print data
                sock.send('Done Processing Data')
            if not data: break
    except KeyboardInterrupt:
        print 'Ending transmission'
        sock.send('end')
        sock.close()
        sys.exit(0)
except socket.error as e:
    print 'Transmitting message failed!'
    print str(e)
    sys.exit(1)

sock.close()


