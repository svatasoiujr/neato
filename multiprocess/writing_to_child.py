from processes import Process, init_signals, close_processes, ProcessStatus
import sys, select, os

process = Process("test_reader.py")

init_signals()
stop = False
while True:
	readable, _, _ = select.select(Process.processes + [sys.stdin], [], [])
	for s in readable:
		if s is sys.stdin:
			l = s.readline()
			if len(l) < 2:
				stop = True
				break
			process.write(l)
			continue
		if s in Process.to_remove:
			Process.to_remove.remove(s)
			continue
		if s is Process.dummy_pipe[0]:
			os.read(s,1)
			Process.to_remove = []
			continue
		if s.status != ProcessStatus.TERMINATED:
			line = s.readline()
			if len(line) > 0:
				s.handle_input(line[:-1])
	if stop == True:
		break

close_processes()