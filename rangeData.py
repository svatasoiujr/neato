class rangeData:
    def __init__(self):
        self.data = dict([(x,[0,0]) for x in range(360)])
    
    def insertData(self, angles, ranges):
        for i in range(len(angles)):
            currData = self.data[angles[i]]
            newCount = currData[1] + 1
            self.data[angles[i]] = [(currData[0]*currData[1] + ranges[i])/newCount, newCount]

    def getValidData(self):
        """ Returns tuple of angles and ranges that have at least one measurement """
        retAngles = []
        retRanges = []
        for i in range(360):
            temp = self.data[i]
            if temp[1] != 0:
                retAngles.append(i)
                retRanges.append(temp[0])
        return (retAngles, retRanges)
