#!/usr/bin/env python

DEVICE = '/dev/ttyACM0'               # Raspbian
#DEVICE = '/dev/tty.usbmodem1d131'    # Mac OS X
#DEVICE = 'DEBUG'                     # Debugging

import sys
import time
import os
import signal
import thread
import serial
import neato_driver as Neato
import Adafruit_BBIO.GPIO as GPIO
import time

# main =========================================================================
  
RANGE = 30

robot = None

class TooClose(Exception):
    pass

try:
    robot = Neato.xv11(DEVICE)
except serial.SerialException:
    print('Unable to connect to XV-11 as device ' + DEVICE)
    print('Make sure XV-11 is powered on and USB cable is plugged in!')
    sys.exit(1)

#turn motor on
print 'ready to start!'
speed = '200'

GPIO.setup("P8_11", GPIO.IN)

while True:
	if GPIO.input("P8_11"):
            print "The button was pressed!"
            try:
                while True:
                    robot.docommand('setmotor lwheeldist 500 rwheeldist 500 speed ' + speed)
                    print 'requesting...'
                    robot.requestScan()
                    [angles,ranges,intensites,errors] = robot.getScanRanges()
                    print 'got ranges: '
                    speed = '200'
                    for i in range(RANGE + 1):
                        angle = i - (RANGE / 2)
                        if errors[angle] < 8000 and ranges[angle] < 550:
                            print "Too Close!!"
                            robot.setMotors(0,0,0)
                            robot.docommand("playsound 0")
                            raise TooClose
                        elif errors[angle] < 8000 and ranges[angle] < 650:
                            print "Kind of close..."
                            speed = '100'
                            break
            except TooClose:
                robot.requestScan()
                [angles,ranges,intensites,errors] = robot.getScanRanges()
                print ranges
                leftRanges  = [ranges[i] for i in range(90 - (RANGE / 2), 90 + (RANGE / 2))]
                rightRanges = [ranges[i] for i in range(270 - (RANGE / 2), 270 + (RANGE / 2))]
                avgLeft  = sum(leftRanges) / RANGE
                avgRight = sum(rightRanges) / RANGE
                print avgLeft
                print avgRight
                if avgLeft < avgRight:
                    robot.docommand("setmotor lwheeldist 200 rwheeldist -200 speed 200")
                else:
                    robot.docommand("setmotor lwheeldist -200 rwheeldist 200 speed 200")
                time.sleep(2)
                robot.docommand('setmotor lwheeldist 300 rwheeldist 300 speed 200')
                time.sleep(2)
                robot.docommand("playsound 1")
	time.sleep(.01)

robot.exit()
sys.exit(0)
