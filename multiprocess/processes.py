import os, sys, signal, select
from time import sleep

class ProcessStatus:
	STOPPED = 1
	RUNNING = 2
	TERMINATED = 3	

main_pid = os.getpid()

class Process:
	dummy_pipe = os.pipe()
	processes = [dummy_pipe[0]]
	to_remove = []

	def __init__(self, f):
		(self.r_child, self.w_parent) = os.pipe()
		(self.r_parent, self.w_child) = os.pipe()
		self.read_file = os.fdopen(self.r_parent)

		self.name = f
		self._file = open(f)
		self._fileno = self._file.fileno()
		self._pipeno = self.read_file.fileno()
		self.pid = 0
		self.status = ProcessStatus.STOPPED
		self.start()
		Process.processes.append(self)
		os.write(Process.dummy_pipe[1], 'x')

	def stop(self):
		os.kill(self.pid, signal.SIGTSTP)
		self.status = ProcessStatus.STOPPED

	def continue_process(self):
		os.kill(self.pid, signal.SIGCONT)
		self.status = ProcessStatus.RUNNING

	def terminate(self):
		os.kill(self.pid, signal.SIGINT)
		self.status = ProcessStatus.TERMINATED

	def kill(self):
		os.kill(self.pid, signal.SIGKILL)
		self.status = ProcessStatus.TERMINATED

	# need for select(), the fileno for the pipe, not for opened program
	def fileno(self):
		return self._pipeno

	def start(self):
		self.pid = os.fork()
		if self.pid == 0:
			# child
			os.dup2(self.r_child, sys.stdin.fileno())
			os.dup2(self.w_child, sys.stdout.fileno())
			os.close(self.w_parent)
			os.close(self.r_parent)
			if self.name[-3:] == ".py":
				exec(self._file)
			else:
				os.execvp(self._file)
			os.kill(main_pid, signal.SIGCHLD)
			sys.exit(1)
		# parent
		self.status = ProcessStatus.RUNNING
		os.close(self.r_child)
		os.close(self.w_child)

	def readline(self):
		return self.read_file.readline()

	def write(self, s):
		os.write(self.w_parent, s)

	def close(self, term=True):
		os.close(self.w_parent)
		if term:
			self.read_file.close()
			self.terminate()
		Process.processes.remove(self)
		Process.to_remove.append(self)
		os.write(Process.dummy_pipe[1], 'x')

	# implement in subclasses
	def handle_input(self, line):
		print("From Child %d: %s" % (self._fileno,line))

def close_processes(a, b):
	for c in Process.processes[1:]:
		c.close()

def sigchld_handler(a, b):
	print "Got a child"
	res = os.wait()
	for p in Process.processes[1:]:
		if p.pid == res[0]:
			p.close(False)
			break
	# print res
	print Process.processes
	print Process.to_remove

def init_signals():
	signal.signal(signal.SIGCHLD, sigchld_handler)
	signal.signal(signal.SIGPIPE, signal.SIG_DFL) 
	signal.signal(signal.SIGINT, close_processes)

def multiplex_io():
	while True:
		readable, _, _ = select.select(Process.processes, [], [])
		for s in readable:
			if s in Process.to_remove:
				Process.to_remove.remove(s)
				continue
			if s is Process.dummy_pipe[0]:
				os.read(s,1)
				Process.to_remove = []
				continue
			if s.status != ProcessStatus.TERMINATED:
				line = s.readline()
				if len(line) > 0:
					s.handle_input(line[:-1])