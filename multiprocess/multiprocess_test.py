from processes import Process, init_signals, close_processes, multiplex_io
from Tkinter import *
import threading, signal
from os import wait

class LinePrint(Process):
	def __init__(self, f):
		Process.__init__(self, f)
		self.lineno = 0

	def handle_input(self, line):
		self.lineno += 1
		print("%d: %s" % (self.lineno, line))

class Doubler(Process):
	def handle_input(self, line):
		print("[%2d]| %s -> %d" % (self._fileno, line, 2*int(line)))

class LidarHandler(Process):
	def handle_input(self, line):
		lsplit = line.split(" ")
		print("%d degrees: %5fmm" % (int(lsplit[0]), float(lsplit[1])))

init_signals()
process_handlers = {	  "child.py": LinePrint, 
					"incrementer.py": Doubler,
					"random_lidar.py": LidarHandler}

class ProcessWidget(Frame):
	def __init__(self, process, plist, master=None):
		Frame.__init__(self, master)
		self.pack({"side": "bottom"})
		self._process = process
		self.master = master

		self.process_label = Label(self, text=process.name)
		self.process_label.pack({"side": "left"})

		self.stop_start_btn = Button(self, text="Pause", fg="yellow")
		self.stop_start_btn["command"] = self.pause_process
		self.stop_start_btn.pack({"side": "left"})

		self.terminate_btn = Button(self, text="Terminate", fg="red")
		self.terminate_btn["command"] = self.kill_process
		self.terminate_btn.pack({"side": "left"})
		self.plist = plist
		plist.append(self)

	def pause_process(self):
		if self.master.just_paused == True:
			return
		self.stop_start_btn["text"] = "Start"
		self.stop_start_btn["fg"] = "Green"
		self.stop_start_btn["command"] = self.continue_process
		self._process.stop()
		self.master.just_paused = True

	def continue_process(self):
		if self.master.just_paused == True:
			return
		self.stop_start_btn["text"] = "Pause"
		self.stop_start_btn["fg"] = "yellow"
		self.stop_start_btn["command"] = self.pause_process
		self._process.continue_process()
		self.pack()
		self.master.just_paused = True

	def kill_process(self, term=True):
		self._process.close(term)
		self.pack_forget()
		self.plist.remove(self)

class Application(Frame):
    def __init__(self, master=None):
        Frame.__init__(self, master)
        self.pack()
        self.create_widgets()
        self.process_widgets = []
        self.just_paused = False

        signal.signal(signal.SIGCHLD, self.my_child_handler)

        # start thread that keeps track of processes
        self.multi_thread = threading.Thread(target=multiplex_io)
        self.multi_thread.daemon = True
        self.multi_thread.start()

    def my_child_handler(self, a, b):
    	if self.just_paused == True:
    		self.just_paused = False
    		return
    	res = wait()
    	for w in self.process_widgets:
			if w._process.pid == res[0]:
				w.kill_process(False)
				break

    def create_widgets(self):
		self.QUIT = Button(self)
		self.QUIT["text"] = "Exit"
		self.QUIT["fg"]   = "red"
		self.QUIT["command"] = self.kill_processes
		self.QUIT.pack({"side": "bottom"})

		self.txtbox = Entry(self)
		self.txtbox.pack({"side": "bottom"})

		self.add_new = Button(self)
		self.add_new["text"] = "Start"
		self.add_new["fg"]   = "green"
		self.add_new["command"] = self.add_process
		self.add_new.pack({"side": "bottom"})

    def kill_processes(self):
    	for pwidg in self.process_widgets:
    		pwidg.kill_process()
    	self.quit()

    def add_process(self):
    	process = None
    	name = self.txtbox.get()
    	if name in process_handlers:
    		process = process_handlers[name](name)
    	else:
    		process = Process(name)
    	widg = ProcessWidget(process, self.process_widgets, self)
        widg.pack()
        self.txtbox.delete(0, END)

root = Tk()
app = Application(master=root)
app.mainloop()
close_processes(0,0)
root.destroy()