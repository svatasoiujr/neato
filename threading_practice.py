#! /usr/bin/env python

import thread
import time

def printNum(delay, word):
    while True:
        time.sleep(delay)
        print(word)

thread.start_new_thread(printNum, (1, "First"))
thread.start_new_thread(printNum, (2, "Second"))

while True:
    pass
    
