neatoDriver::neatoDriver()
{
  Serial.begin(115200 / 8);
  doCommand("testmode on");
  doCommand("playsound 0");
}

neatoDriver::doCommand(char* command)
{
  Serial.write(command);
}

neatoDriver::exit()
{
  doCommand("testmode off");
}
