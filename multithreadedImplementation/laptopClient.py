#!/usr/bin/env python
# Laptop Server

import socket
import thread
from Tkinter import *
import matplotlib
from notify.all import *
import pylab
import Queue
import sys
import time
import threading
from math import cos,sin

HOST = '192.168.7.2'
PORT = 5010

sock = None

def initConnections():
    global sock
    try:
        # Connect to host over port
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect((HOST, PORT))
    except socket.error as err:
        print 'Unable to connect to BeagleBone: ' + str(err)
    sock.send('Connection established, from laptop')
    print(sock.recv(64))

initConnections()

class myGUI(Tk):
    def __init__(self):
        Tk.__init__(self)
        self.currentCommand = Variable()
        self.currentCommand.value = ''
        self.currentCommand.changed.connect(self.ccChange)
        thread.start_new_thread(self.dataThread, ())
        self.q = Queue.Queue()
        self.after(50, self.checkQueue)
        self.startUp()

    def dataThread(self):
        while True:
            try:
                data = sock.recv(1024)
                if data:
                    self.q.put(data)
            except:
                continue
                
    def checkQueue(self):
        try:
            data = self.q.get(block=False)
        except Queue.Empty:
            self.after(100, self.checkQueue)
        else:
            print data
            self.checkQueue()

    #@staticmethod
    def ccChange(self,c):
        if c != "":
            print c
            sock.send(c)
            self.currentCommand.value  = ""
            self.content.set("")
        
    def upC(self):
        self.currentCommand.value = "setmotor lwheeldist 200 rwheeldist 200 speed 100"

    def downC(self):
        self.currentCommand.value = "setmotor lwheeldist -200 rwheeldist -200 speed 100"

    def leftC(self):
        self.currentCommand.value = "setmotor lwheeldist -200 rwheeldist 200 speed 100"

    def rightC(self):
        self.currentCommand.value = "setmotor lwheeldist 200 rwheeldist -200 speed 100"
        
    def endC(self):
        self.currentCommand.value = "end"

    def ldsC(self):
        self.currentCommand.value = "getldsscan"

    def sendC(self):
        self.currentCommand.value = self.content.get()

    def startUp(self):
        self.title("Laptop Client Window")
        self.geometry('450x300+200+200')
        self.content = StringVar()
        self.commEnt = Entry(self, textvariable=self.content)
        upButton = Button(self, text='Up',command=self.upC)
        downButton = Button(self, text='Down',command=self.downC)
        leftButton = Button(self, text='Left',command=self.leftC)
        rightButton = Button(self, text='Right',command=self.rightC)
        endButton = Button(self, text='End',command=self.endC)
        ldsButton = Button(self, text='LDS', command=self.ldsC)
        sendButton = Button(self, text='Send', command=self.sendC)
        upButton.place(x=205,y=50)
        downButton.place(x=200,y=100)
        leftButton.place(x=170,y=75)
        rightButton.place(x=230,y=75)
        endButton.place(x=0,y=0)
        ldsButton.place(x=0,y=100)
        self.commEnt.place(x=0,y=30)
        sendButton.place(x=0,y=50)
        self.mainloop()
        sys.exit(0)
    
app = myGUI()


while True:
    pass

