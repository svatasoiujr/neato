#!/usr/bin/env python

DEVICE = '/dev/ttyACM0'               # Raspbian
#DEVICE = '/dev/tty.usbmodem1d131'    # Mac OS X
#DEVICE = 'DEBUG'                     # Debugging

import sys
import time
import os
import signal
import thread
import serial
import neato_driver as Neato
import socket

HOST = ''
PORT = 5010
# main =========================================================================
sock = None   
robot = None
conn = None

def initConnections():
    global sock, robot, conn
    while True:
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            
        try:
            sock.bind((HOST, PORT))
            break
                
        except socket.error as err:
            print('Bind failed: ' + str(err))
                
        time.sleep(1)
            
    sock.listen(1) 
    
    print('Waiting for client to connect...')
    try:
        conn, addr = sock.accept()
        print('Accepted connection')
    except:
        sys.exit(1)

    data = conn.recv(1024)
    print data
    if not data: sys.exit(1)
    #process data...
    conn.send('Welcome, from Beaglebone!')
    conn.send('Starting to transmit data!')

    if DEVICE != 'DEBUG':
        try:
            robot = Neato.xv11(DEVICE) 
        except serial.SerialException:
            print('Unable to connect to XV-11 as device ' + DEVICE)
            print('Make sure XV-11 is powered on and USB cable is plugged in!')
            sys.exit(1)

def dataThread():
    """Waits for data from Neato, then relays them to laptop"""
    while True:
        try:
            neatoData = robot.port.readline()
            if neatoData:
                conn.send(neatoData)
                print neatoData
        except:
            continue

def commandThread():
    """Waits for commands from laptop, then relays them to Neato commands"""
    def processData(string):
        """Given a string from the laptop, decides appropriate output, be it a command to the Neato or just nothing"""
        if string == 'end':
            myexit()
        else:
            robot.docommand(string)
            
    while True:
        try:
            data = conn.recv(1024)
            if not data:
                break
            print(data)
            processData(data)
        except:
            continue
    myexit()
    #os.kill(os.getpid(), signal.SIGINT)

def myexit():
    robot.exit()
    conn.close()
    sock.close()
    #sys.exit(1)

initConnections()
thread.start_new_thread(dataThread, ())
thread.start_new_thread(commandThread, ())

while True:
    pass
