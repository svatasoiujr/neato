#!/usr/bin/env python

DEVICE = '/dev/ttyACM0'               # Raspbian
#DEVICE = '/dev/tty.usbmodem1d131'    # Mac OS X
#DEVICE = 'DEBUG'                     # Debugging

import sys
import os
import serial
import neato_driver as Neato
import socket

HOST = ''
PORT = 5010
# main =========================================================================

# Default to no robot (test mode)
robot = None
while True:
    sock = None   
    
    while True:
    
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    
        try:
            sock.bind((HOST, PORT))
            break
        
        except socket.error as err:
            print('Bind failed: ' + str(err))
        
        time.sleep(1)
        
    sock.listen(1) 

    # Accept a connection from a client
    print('Waiting for client to connect ...')
    try:
        conn, addr = sock.accept()
        print('Accepted connection')
    except:
        break
    
    #conn, addr = sock.accept()
    while True:
        data = conn.recv(1024)
        print data
        if not data: break
        #process data...
        conn.send('Welcome, from Beaglebone!')
        conn.send('Starting to transmit data!')

        if DEVICE != 'DEBUG':
            try:
                robot = Neato.xv11(DEVICE) 
            except serial.SerialException:
                print('Unable to connect to XV-11 as device ' + DEVICE)
                print('Make sure XV-11 is powered on and USB cable is plugged in!')
                sys.exit(1)

            #start motor
            print 'Moving left motor -200, right motor 200 at speed 200'
            robot.docommand('setmotor lwheeldist -200 rwheeldist 200 speed 200')
            # get version
            print 'Getting Version'
            robot.getVersionData()
            try:
                while data != 'end':
                    conn.send('Starting Scan')
                    print 'getting data...'
                    robot.requestScan()
                    [angles,ranges,intensites,errors] = robot.getScanRanges()
                    conn.send('Sending Scan Data')
                    print 'sending data...'
                    #for angle in range(len(angles)):
                        #print str(angle) + " | " + str(ranges[angle]) + "mm" + " | " + str(intensites[angle]) + " | " + str(errors[angle])
                    #ranges = map(lambda r: int(r), ranges) 
                    conn.send('Sending Angles')
                    conn.send(str(angles))
                    print conn.recv(32)
                    conn.send('Sending Ranges')
                    conn.send(str(ranges))
                    conn.send('Done sending ranges')
                    print conn.recv(32)
                    conn.send('Sending Errors')
                    conn.send(str(errors))
                    #conn.send('Done sending errors')
                    print conn.recv(32)
                    data = conn.recv(32)
                    print data
                    if data == 'Done Processing Data':
                        pass
                robot.exit()
                conn.close()
                sock.close()
                sys.exit(0)
            except socket.error as err:
                print 'Connection ended'
                robot.exit()
                conn.close()
                sock.close()
                sys.exit(0)
        else:
            print('No robot connected; running in test mode')
    
    conn.close()
    sock.close()

# DEBUG is special name to enable debugging    

